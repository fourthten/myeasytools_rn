import React, { Component } from 'react';
import { Platform, StyleSheet, Text, TouchableHighlight, TouchableOpacity, TouchableNativeFeedback, TouchableWithoutFeedback, View } from 'react-native';

export default class ProfileScreen extends Component {
    render(){
        return (
            <View style={styles.container}>
                <Text>This is Jane's profile</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      paddingTop: 60,
      alignItems: 'center'
    },
});